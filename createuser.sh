#!/bin/bash
# cron job for linux

# Shell script to create users in Linux

# This checks if you are a root user, id=0 means you are a root user
if [ $(id -u) -eq 0 ]; then
read -p "Enter the username : " user
read -s -p "Enter the password : " passw

# This searches the user in the passwd file for its existence
egrep -w "^$user" /etc/passwd >/dev/null
if [ $? -eq 0 ]; then
echo "$user already exists"
exit 1
else

# password will be encrypted and saved to the system
pass=$(perl -e 'print crypt($ARGV[0], "password")' $passw)
useradd -m -p $pass $user
[ $? -eq 0 ] && echo "User has been added to system." || echo "Failed to add user."
fi
else

# If you are not root you will not be allowed to create new user and password.
echo "Only root is permitted to add user to the system"
exit 2
fi
