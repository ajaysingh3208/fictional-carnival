# fictional-carnival

This is a list of some Important shell scripts [cron job for Linux Admin](http://www.techsakh.com/2016/01/18/top-cron-job-linux-admin/) and [sed command examples in UNIX and Linux](http://www.techsakh.com/2016/05/12/20160512use-sed-command-linux-unix/) shell script from Techsakh.

1.Shell script to backup files everyday.
[dailyscript.sh](https://github.com/saby175/fictional-carnival/blob/master/dailyscript.sh)

2.Shell script to check computer performance.
[performa.sh](https://github.com/saby175/fictional-carnival/blob/master/performa.sh)

3.Shell script to send email when disk space exceed limits.
[diskmail.sh](https://github.com/saby175/fictional-carnival/blob/master/diskmail.sh)

4.Shell script to create user with password.
[createuser.sh](https://github.com/saby175/fictional-carnival/blob/master/createuser.sh)