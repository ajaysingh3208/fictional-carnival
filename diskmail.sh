#!/bin/bash
# cron job for linux

# Diskspace will be stored in DISKSPACE and check if its more than 85 %
DISKSPACE=$(df / | grep / | awk '{ print $5}' | sed 's/%//g')
MAXIMUM=85

# If diskspace is found more than MAXIMUM a alert will be triggered.

if [ "$DISKSPACE" -gt "$MAXIMUM" ] ; then
mail -s 'Disk Space Very Low!!' techsakh@techsakh.local << EOF
Hello! Your Disk space is very low. Current Used Disk Space is = $DISKSPACE%
EOF
fi
~
~
~
