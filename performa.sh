#!/bin/bash
# cron job for linux

echo -e "\e[1;33m-------------- FREE MEMORY AND USED MEMORY -------------------------\e[0m"
free -h
echo ""
echo -e "\e[1;33m-------------- FREE DISK SPACE -------------------------------------\e[0m"
df -h
echo ""
echo -e "\e[1;33m-------------- USERS LOGGED IN AND LOAD AVERAGE --------------------\e[0m"
w
echo ""
echo -e "\e[1;33m-------------- ACTIVE CONNECTIONS ---------------------------------\e[0m"
netstat -tlnp
echo ""
~ 
